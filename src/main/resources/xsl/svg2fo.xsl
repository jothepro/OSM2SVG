<xsl:stylesheet version="2.0" xmlns:fo="http://www.w3.org/1999/XSL/Format"
                             xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                             xmlns:fn="http://www.w3.org/2005/xpath-functions"
                             xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output method="xml" version="1.0" indent="yes"/>


	<xsl:template match="/">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format"> <!--that tag is always the same in a xslt-fo file-->
			<fo:layout-master-set>
				<!-- defining a layout within the master set-->
				<fo:simple-page-master page-height="297mm" page-width="210mm" margin="5mm 10mm 5mm 10mm" master-name="PageMaster">
					<fo:region-body   margin="0mm 0mm 0mm 0mm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>

			<fo:page-sequence master-reference="PageMaster">
				<!--applying the layout to the body region of the pdf -->
				<fo:flow flow-name="xsl-region-body" >
					<fo:block>
						<fo:instream-foreign-object content-width="190mm" xmlns:svg="http://www.w3.org/2000/svg">
							<xsl:copy-of select="/"/>
						</fo:instream-foreign-object>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>

	</xsl:template>


</xsl:stylesheet >