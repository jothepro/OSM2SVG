package osm;

import java.io.Serializable;
import java.util.Locale;

/**
 * Defines which Element in the Map are rendered.
 * If a categorie of Element is set to false, it isnt queried from the Overpass API and in consequese isn't rendered
 */
public class Filter implements Serializable {

    private double lat;
    private double lon;
    private double zoom;
    private boolean streets;
    private boolean buildings;
    private boolean nature;


    public Filter() {
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setZoom(double zoom) {
        this.zoom = zoom;
    }

    public void setBuildings(boolean show) { this.buildings = show; }

    public void setStreets(boolean show) { this.streets = show; }

    public void setNature(boolean show) { this.nature = show; }


    public double getLeft() {
        return this.lon - this.zoom;
    }

    public double getBottom() {
        return this.lat - this.zoom;
    }

    public double getRight() {
        return this.lon + this.zoom;
    }

    public double getTop() {
        return this.lat + this.zoom;
    }


    /**
     * builds the Overpass Query in "Overpass QL" Format
     * @see <a href="http://wiki.openstreetmap.org/wiki/Overpass_API">Overpass API</a>
     * @return Overpass QL
     */
    public String getOverpassQuery() {
        String query = "(";
        if(buildings) {
            query += String.format(
                    Locale.ENGLISH,
                    "way[\"building\"]" +
                    "(%f, %f, %f, %f);" +
                    ">;",
                    this.getBottom(),
                    this.getLeft(),
                    this.getTop(),
                    this.getRight());
        }
        if(nature) {
            query += String.format(
                    Locale.ENGLISH,
                    "way[\"landuse\"]" +
                            "(%f, %f, %f, %f);" +
                            ">;",
                    this.getBottom(),
                    this.getLeft(),
                    this.getTop(),
                    this.getRight());
        }
        if(streets) {
            query += String.format(
                    Locale.ENGLISH,
                    "way[\"highway\"]" +
                            "(%f, %f, %f, %f);" +
                            ">;",
                    this.getBottom(),
                    this.getLeft(),
                    this.getTop(),
                    this.getRight());
        }

        query += ");" +
                "out;";
        return query;
    }

}
